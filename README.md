# Strong-Dangerous - MarvelAPI

It is an application where you can search for your favorite character from the Marvel universe. It also shows you which Comics it is related to.

## Authors

- Danelia Sanchez en [GitHub](https://github.com/DaneliaSanchz) y en
  [LinkedIn](https://www.linkedin.com/in/danelia-sanchez/)
- Javier Jimenez en [GitHub](https://github.com/javitecsis) y en
  [LinkedIn](https://www.linkedin.com/in/javitecsis/)
- Jorge Arias Argüelles en [GitHub](https://github.com/jorgearguellles) y en
  [LinkedIn](https://www.linkedin.com/in/jorgeariasarguelles/)

## Documentation

Make with :green_heart: and:

- [Styles con SASS](https://sass-lang.com)
- [Components & logic con React.js](https://es.reactjs.org)
- [Data from Marvel API](https://developer.marvel.com)
<!-- - [Desplegada en AWS Amplify](https://aws.amazon.com) -->

## Demo

- coming zoon ...
<!-- [Pruébala](https://main.d32onoc11tul47.amplifyapp.com) -->

## ScreenShot

- coming zoon ...
<!-- [App Screenshot](https://github.com/jorgearguellles/weatherApp/blob/main/src/img/previewPAge.png) -->

## Feedback

If you find a way to improve our project, please feel free to share whit: [@DaneliaSanchez](https://www.linkedin.com/in/danelia-sanchez/), [@JavierJimenez](https://www.linkedin.com/in/javitecsis/),[@JorgeAriasArgüelles](https://www.linkedin.com/in/jorgeariasarguelles/)
